import { shallowMount } from '@vue/test-utils';
import Logo from '../../../src/components/logo.vue';

describe('Logo.vue', () => {
  it('renders main components', () => {
    const wrapper = shallowMount(Logo);
    expect(wrapper.element).toMatchSnapshot();
  });
});
