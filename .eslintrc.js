const isProdOrCI = () => ["production", "CI"].includes(process.env.NODE_ENV);

module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["@vue/prettier", "plugin:@gitlab/default"],
  parserOptions: {
    parser: "babel-eslint"
  },
  rules: {
    "no-console": isProdOrCI() ? "error" : "off",
    "no-debugger": isProdOrCI() ? "error" : "off",
    "prettier/prettier": "error"
  },
  overrides: [
    {
      files: [
        "**/__tests__/*.{j,t}s?(x)",
        "**/tests/unit/**/*_spec.{j,t}s?(x)"
      ],
      env: {
        jest: true
      }
    }
  ]
};
